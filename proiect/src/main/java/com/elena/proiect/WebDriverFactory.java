package com.elena.proiect;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.GeckoDriverService;

import java.util.HashMap;
import java.util.Map;

public final class WebDriverFactory {

    private static final String OS_PROP_NAME = "os.name";

    private enum OperatingSystem {
        WIN,
        LINUX,
        MAC,
        ;
    }

    private static final OperatingSystem OS = detectOS();

    private static final Map<WebDriverType, WebDriverCreator> creators = initCreators();

    private WebDriverFactory() {
    }

    public static WebDriver create(WebDriverType type) {
        WebDriverCreator webDriverCreator = creators.get(type);
        if (webDriverCreator == null) {
            throw new RuntimeException("WebDriverCreator  not available for type " + type.name());
        }

        return webDriverCreator.create();
    }

    private static OperatingSystem detectOS() {
        String osName = System.getProperty(OS_PROP_NAME).toLowerCase();
        if (osName.contains(OperatingSystem.MAC.name().toLowerCase())) {
            return OperatingSystem.MAC;
        }
        if (osName.contains(OperatingSystem.WIN.name().toLowerCase())) {
            return OperatingSystem.WIN;
        }
        return OperatingSystem.LINUX;
    }

    private static Map<WebDriverType, WebDriverCreator> initCreators() {
        Map<WebDriverType, WebDriverCreator> result = new HashMap<>();
        result.put(WebDriverType.CHROME, ChromeWebDriver.INSTANCE);
        result.put(WebDriverType.FIREFOX, FirefoxWebDriver.INSTANCE);
        return result;
    }

    private interface WebDriverCreator {
        WebDriver create();
    }

    private static final class ChromeWebDriver implements WebDriverCreator {

        private static final ChromeWebDriver INSTANCE = new ChromeWebDriver();

        private static final Map<OperatingSystem, String> chromeLocations = initLocations();

        private ChromeWebDriver() {
        }

        @Override
        public WebDriver create() {
            String chromeDriverLocation = chromeLocations.get(OS);
            if (chromeDriverLocation == null) {
                throw new RuntimeException("Please configure bellow init location for your OS: " + OS.name());
            }
            System.setProperty("webdriver.chrome.driver", chromeDriverLocation);
            return new ChromeDriver();
        }

        private static Map<OperatingSystem, String> initLocations() {
            Map<OperatingSystem, String> result = new HashMap<>();
            result.put(OperatingSystem.MAC, "/Applications/Google\\ Chrome.app/Contents/MacOS/Google\\ Chrome");
            result.put(OperatingSystem.WIN, "C:\\drivers\\chromedriver.exe");
            result.put(OperatingSystem.LINUX, "/usr/bin/google-chrome");
            return result;
        }
    }

    private static final class FirefoxWebDriver implements WebDriverCreator {

        private static final FirefoxWebDriver INSTANCE = new FirefoxWebDriver();

        private static final Map<OperatingSystem, String> geckoLocations = initLocations();

        private FirefoxWebDriver() {
        }

        @Override
        public WebDriver create() {
            String geckoDriverLocation = geckoLocations.get(OS);
            if (geckoDriverLocation == null) {
                throw new RuntimeException("Please configure bellow init location for your OS: " + OS.name());
            }
            System.setProperty("webdriver.gecko.driver", geckoDriverLocation);
            return new FirefoxDriver();
        }

        private static Map<OperatingSystem, String> initLocations() {
            Map<OperatingSystem, String> result = new HashMap<>();
            result.put(OperatingSystem.WIN, "C:\\drivers\\geckodriver.exe");
            return result;
        }
    }
}
