package com.elena.proiect;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleSearchTest {

    private TestConfiguration testConfiguration = TestConfiguration.newInstance()
            .setWebDriverType(WebDriverType.FIREFOX)
            .setUrl("https://www.google.com/")
            .setTimeout(20);

    private WebDriver webDriver;

    @Test
    public void testVerifyPageData() {
        String expectedTitle = "Google";
        String actualTitle = webDriver.getTitle();
        Assert.assertEquals(expectedTitle, actualTitle);

        WebElement element = webDriver.findElement(By.name("q"));
        element.sendKeys("Cheese!\n", Keys.ENTER);

        waitForPageToLoad("search");

        WebElement linkToFollow = webDriver.findElement(By.xpath("(//div[@class='r']/a)[2]"));
        linkToFollow.click();

        waitForPageToLoad("top-banner");

        WebElement firstPageIntro = webDriver.findElement(By.xpath("//*[@id=\"intro-text\"]/p[1]"));
        String firstPageIntroText = firstPageIntro.getText();

        String expectedIntroTextContains = "milk";
        Assert.assertTrue(firstPageIntroText.toLowerCase().contains(expectedIntroTextContains));
        System.out.println(firstPageIntroText);
    }

    @Test
    public void testVerifyPageData2() {
        String expectedTitle = "Google";
        String actualTitle = webDriver.getTitle();
        Assert.assertEquals(expectedTitle, actualTitle);

        WebElement element = webDriver.findElement(By.name("q"));
        element.sendKeys("selenium tutorial java", Keys.ENTER);

        waitForPageToLoad("search");

        WebElement linkToFollow = webDriver.findElement(By.xpath("(//div[@class='r']/a)[1]"));
        linkToFollow.click();

        waitForPageToLoad("g-container-main");

        WebElement someTutorial = webDriver.findElement(By.xpath("//*[@id='g-mainbar']/div/div/div/div/div/div/div[2]/table[1]/tbody/tr[2]/td[1]/a"));
        someTutorial.click();

        sleep(20000);
    }

    private void waitForPageToLoad(String expectedId) {
        waitForPageToLoad(expectedId, testConfiguration.getTimeout());
    }

    private void waitForPageToLoad(String expectedId, long timeoutInSeconds) {
        new WebDriverWait(webDriver, timeoutInSeconds).until(ExpectedConditions.presenceOfElementLocated(By.id(expectedId)));
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Before
    public void testSetup() {
        webDriver = WebDriverFactory.create(testConfiguration.getWebDriverType());
        webDriver.get(testConfiguration.getUrl());
    }

    @After
    public void testTearDown() {
        webDriver.quit();
//        webDriver.close();
        webDriver = null;
    }

}
