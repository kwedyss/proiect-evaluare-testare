package com.elena.proiect;

public final class TestConfiguration {

    private WebDriverType webDriverType = WebDriverType.CHROME;
    private String url;
    private long timeout = 10;

    private TestConfiguration() {}

    public static TestConfiguration newInstance() {
        return new TestConfiguration();
    }

    public TestConfiguration setWebDriverType(WebDriverType webDriverType) {
        this.webDriverType = webDriverType;
        return this;
    }

    public WebDriverType getWebDriverType() {
        return webDriverType;
    }

    public TestConfiguration setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public TestConfiguration setTimeout(long timeout) {
        this.timeout = timeout;
        return this;
    }

    public long getTimeout() {
        return timeout;
    }
}
