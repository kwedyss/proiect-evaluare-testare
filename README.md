# proiect-evaluare-testare

# Instalare

**Requirements**

_Assuming git is installed_ 
Java JDK 11: https://www.oracle.com/java/technologies/javase-jdk11-downloads.html
Maven: https://maven.apache.org/download.cgi
Maven install documentation: https://maven.apache.org/install.html

Selenium tool documentation: https://www.selenium.dev/documentation/en/
Selenium tutorials: https://www.guru99.com/selenium-tutorial.html
https://www.javatpoint.com/selenium-tutorial

Selenium stie sa:
* deschida un browser; acel browser trebuie sa fie instalat pe masina pe care rulezi selenium
** daca pe masina ai Firefox browser: ai nevoie sa downloadezi geckodriver:
*** windows x32: https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-win32.zip
*** windows x64 :https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-win64.zip
Extragi continutul arhivei intr-o locatie pe disk (ex: C:\drivers)
Setezi calea catre driver in WebDriverFactory.FirefoxWebDriver.initLocations, pt sistemul tau de operare
In GoogleSearchTest.testConfiguration alegi FIREFOX web driver: setWebDriverType(WebDriverType.FIREFOX)

** daca pe masina ai Chrome browser: ai nevoie sa downloadezi chrome driver de aici: https://chromedriver.chromium.org/downloads
ATENTIE: trebuie sa downloadezi versiunea potrivita de driver
*** chrome version 79 download: https://chromedriver.storage.googleapis.com/index.html?path=79.0.3945.36/
*** chrome version 80 download: https://chromedriver.storage.googleapis.com/index.html?path=80.0.3987.106/
Din nou, extragi continutul arhivei intr-o locatie pe disk (ex: C:\drivers)
Setezi calea catre driver in WebDriverFactory.ChromeWebDriver.initLocations, pt sistemul tau de operare
In GoogleSearchTest.testConfiguration alegi CHROME web driver: setWebDriverType(WebDriverType.CHROME)

* navigheze la o anume pagina folosind browserul selectat:
webDriver.get("http://somepage.com");
NOTE: testul GoogleSearchTest este configurat sa initializeze robotul, sa lanseze browserul si sa incarce pagina configurata in GoogleSearchTest.testConfiguration.url, inaintea fiecarui test.
La finalul testului va inchide tot, iar la testul urmator va reface pasii anteriori

* sa verifice diverse informatii de pe pagina incarcata, prin cautarea unui element folosind XPath




